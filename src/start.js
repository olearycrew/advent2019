const fs = require("fs");

const day1 = require("./day01");
const day2 = require("./day02");

module.exports = function (day) {
    switch (day) {
        case 1:
            day1.part1()
            .then(fuelNeed => console.log(fuelNeed))
            .catch(e => { throw e });
            break;
        case 1.5:
            day1.part2()
            .then(fuelNeed => console.log(fuelNeed))
            .catch(e => { throw e });
            break;
        case 2:
            day2.part1()
            .then(fuelNeed => console.log(fuelNeed))
            .catch(e => { throw e });
            break;
        case 2.5:
            day2.part2()
            .then(fuelNeed => console.log(fuelNeed))
            .catch(e => { throw e });
            break;
        default:
            break;
    }
}

