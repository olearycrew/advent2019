const data = require('../lib/data');

module.exports = {
    part1,
    part2
}

function part1() {
    return new Promise((resolve, reject) => {
        data.day2input()
        .then(val => {
            const iArr = val.split(",");
            let currOpcodeP = 0;
            let haltandcatchfire = false;

            while (currOpcodeP < iArr.length && !haltandcatchfire) {
                const opcodeS  = iArr[currOpcodeP];
                let opcode = parseInt(opcodeS)
                if (opcode == 99) {
                    haltandcatchfire = true;
                    resolve(iArr)
                } else {
                    const inputA  = iArr[currOpcodeP + 1];
                    const inputB  = iArr[currOpcodeP + 2];
                    const outputP = iArr[currOpcodeP + 3];

                    let factorA = parseInt(iArr[inputA]);
                    let factorB = parseInt(iArr[inputB]);

                    switch (opcode) {
                        case 1:
                            //Addition
                            iArr[outputP] = factorA + factorB;
                            break;
                        case 2:
                            // Multiplication
                            iArr[outputP] = factorA * factorB
                            break;
                        default:
                            throw new Error(`Something went wrong.  Opcode ${opcode} at iArr[${currOpcodeP}]`)
                            break;
                    }
                    currOpcodeP += 4;
                    //console.log(currOpcodeP)
                }
            }

            resolve(iArr)
        })
        .catch(e => reject(e));
    });
}

function part2() {

}