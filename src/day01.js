const data = require('../lib/data');
const fuel = require('../lib/fuel');

module.exports = {
    part1,
    part2
}

function part1() {
    return new Promise((resolve, reject) => {
        data.day1input()
        .then(val => {
            const inputs = val.split("\n");
            const totalFuel = inputs.reduce((acc, item) => acc + fuel.getFuelNeed(item), 0);
            resolve(totalFuel)
        })
        .catch(e => reject(e));
    });
}

function part2() {
    return new Promise((resolve, reject) => {
        data.day1input()
        .then(val => {
            const inputs = val.split("\n");
            let totalFuel = 0;

            inputs.forEach(item => {
                const modF = fuel.getFuelNeed(item)
                const ff = fuel.getFuelFuel(modF);
                totalFuel += modF + ff;
            });

            resolve(totalFuel)
        })
        .catch(e => reject(e));
    })
}
