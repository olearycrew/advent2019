const fs = require("fs");

module.exports = {
    day1input() {
        return new Promise((resolve, reject) => {
            try {
                const input = fs.readFileSync('./inputs/day1.txt')
                resolve(input.toString());
            } catch (error) {
                reject(error);
            }
        })
    },
    day2input() {
        return new Promise((resolve, reject) => {
            try {
                const input = fs.readFileSync('./inputs/day2.txt')
                resolve(input.toString());
            } catch (error) {
                reject(error);
            }
        })
    }
}