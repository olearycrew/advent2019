module.exports = {
    getFuelNeed,
    getFuelFuel,
}

function getFuelNeed(mass) {
    let fuel = mass / 3
    fuel = Math.floor(fuel)
    return fuel - 2
}

function getFuelFuel(fuel) {
    let lastFuel = fuel;
    let fuelFuel = 0;

    while (lastFuel > 0) {
        lastFuel = getFuelNeed(lastFuel);
        if (lastFuel > -1) fuelFuel += lastFuel;
    }
    return fuelFuel;
}